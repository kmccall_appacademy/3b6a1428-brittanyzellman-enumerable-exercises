require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |word| sub_string?(word, substring) }
end

def sub_string?(word, substring)
  return true if word.include?(substring)
  false
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  char_in_str = string.chars.uniq
  char_in_str.select do |ch|
    string.count(ch) > 1 && ch != " "
  end
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  longest = ""
  long = ""
  arr_words = string.split(" ")
  arr_words.each do |word|
    if word.length > longest.length
      long = longest
      longest = word
    elsif word.length > long.length && word.length < longest.length
      long = word
    end
  end
  arr_words.select { |w| w == longest || w == long }
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alpha = ("a".."z").to_a
  string_chars = string.chars.uniq
  alpha.reject { |ch| string_chars.include?(ch) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  # debugger
  (first_yr..last_yr).select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  return true if year.to_s.chars == year.to_s.chars.uniq
  false
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  singles = no_repeats(songs)
  singles.uniq
end

def no_repeats(songs)
  songs.each_index do |i|
    if songs[i] == songs[i+1]
      songs.delete(songs[i])
    end
  end
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.delete!(".,?;:!")
  arr_words = string.split(" ")
  arr_words = arr_words.select { |word| word.include?("c") }
  i = -1
  arr_words.each do |word|
    return word if word[i] == "c"
    i -= 1
  end
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  return_arr = []
  i = 0
  while i < arr.length
    j = i + 1
    while arr[i] == arr[j]
      j += 1
    end
    if j > i + 1
      return_arr << [i, j - 1]
    end
    i = j
  end
  return_arr
end
